# Classic McEliece goes to IETF

IETF document for Classic McEliece, see https://classic.mceliece.org/
and in particular https://classic.mceliece.org/iso.html on which this
work is based.

The latest output can be read here:
https://jas.gitlab.io/ietf-mceliece

Submitted versions can be found here:
https://datatracker.ietf.org/doc/draft-josefsson-mceliece/
